import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.screenshot;

import org.junit.*;
import org.junit.Test;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;


import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class JUnitTest  {

    @BeforeClass
    public static void hiTest() throws InterruptedException {
        open("https://image.shutterstock.com/image-vector/lets-do-it-hand-drawn-260nw-336878684.jpg");
        Thread.sleep(1000);
    }

    @Before
    public void beforeTests() throws InterruptedException {
        open("https://media2.giphy.com/media/RrVzUOXldFe8M/200.webp?cid=790b76115cda9c0b644638514111bed7&rid=200.webp");
        Thread.sleep(1500);
    }

    @Disabled("for JUnit5")
    public void firstTest() {

        open("https://www.rabota.ua/");
        $("#ctl00_Header_header > div > header > div > a > img.f-header-logo-symbol-red").shouldBe(visible);
        $("#gnav_login").shouldBe(visible);
        $("#ctl00_Header_header > div > header > div > div > ul > li:nth-child(4) > a.f-header-menu-list-link-with-border > label > i").shouldBe(visible);
        $("#ctl00_content_vacSearch_Keyword").shouldHave(text("Бухгалтер"));
        $("#aspnetForm > footer > div.f-footer--primary > div > div > div > a").shouldBe(visible);
        $("#aspnetForm > footer > div.f-bg-white.f-footer--secondary > div > div > a:nth-child(1) > img:nth-child(2)").shouldBe(visible);
        $("#ctl00_content_imgLogo").shouldHave(text("Нужные люди в нужном месте"));
        screenshot("Test_1");
    }

    @Test
    public void secondTest(){

        open("https://www.rabota.ua/");
        $(By.xpath("//*[@id=\"ctl00_Header_header\"]/div/header/div/div/ul/li[4]/a[1]/label")).click();
        $(By.xpath("//*[@id=\"ctl00_Sidebar_login_txbLogin\"]")).shouldBe(visible);
        $(By.xpath("//*[@id=\"ctl00_Sidebar_login_txbPassword\"]")).shouldBe(visible);
        $(By.xpath("//*[@id=\"ctl00_Sidebar_login_lnkLogin\"]")).shouldBe(visible);
        screenshot("Test_2");
    }
    @Test
    public void thirdTest(){
        open("https://www.rabota.ua/");
        $(By.xpath("//*[@id=\"aspnetForm\"]/footer/div[1]/div/div/nav[1]/div/ul/li[1]/span/label")).click();
        $(By.xpath("//*[@id=\"ctl00_Header_header\"]/div/header/div/div/ul/li[4]/a[2]/span")).shouldBe(visible);
        $(By.xpath("//*[@id=\"ctl00_Header_header\"]/div/header/div/div/ul/li[4]/a[1]/label")).shouldHave(text("Увійти"));
        screenshot("Test_3");

    }
    @Test
    public void fourthTest(){
        open("https://www.rabota.ua/ua");
        $(By.xpath("//*[@id=\"ctl00_content_vacSearch_CityPickerWork_inpCity\"]")).setValue("Харьков").pressEnter();
        $(By.xpath("//div/div[3]//div[3]/input")).shouldBe(visible);
        screenshot("Test_4");
    }
    @Test
    public void fifthTest(){
        open("https://www.rabota.ua/");
        $(By.xpath("//*[contains(@id,\"Search_Keyword\")]")).setValue("QA automation engineer").pressEnter();
        //$(By.xpath("//*[@id=\"7454160\"]/td/article/div[1]/div/h3/a")).waitUntil(exist,1000);
        $(By.xpath("//input[@placeholder='Введите ключевые слова']")).shouldBe(visible);
        screenshot("Test_5");

    }

    @After
    public void aftTests() throws InterruptedException {
        open("https://previews.123rf.com/images/arcady31/arcady311303/arcady31130300004/18233954-good-job-smiley.jpg");
        Thread.sleep(1000);
    }

    @AfterClass
    public static void byTest() throws InterruptedException {
        open("https://media1.tenor.com/images/834a4e75ad85fd24c538e38a8c072698/tenor.gif?itemid=8184785");
        Thread.sleep(1000);
    }


}